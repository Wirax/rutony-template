using System;
using System.Collections.Generic;

namespace RutonyChat.tools {
    public static class ConstantStorage {
        public static string site;
        public static string username;
        public static string text;
        public static string param;
        public static Dictionary<string, string> DictionaryParams;

        public static void initStorage(string site, string username, string text, string param) {
            ConstantStorage.site = site;
            ConstantStorage.username = username;
            ConstantStorage.text = text;
            ConstantStorage.param = param;
        }
        
        public static void initStorage(string site, string username, string text, Dictionary<string, string> dictionaryParams) {
            ConstantStorage.site = site;
            ConstantStorage.username = username;
            ConstantStorage.text = text;
            ConstantStorage.DictionaryParams = dictionaryParams;
        }
    }
}