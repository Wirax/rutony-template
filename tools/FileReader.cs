using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace RutonyChat.tools {
    public class FileReader {
        public static List<String> readAsStrings(String filename) {
            List<String> stringsList = new List<String>();

            if (!RutonyBotFunctions.FileExists(filename))
                DebugWriter.writeError(String.Format("[Read Exception] - Source file {0} not found", filename));

            int stringsAmount = RutonyBotFunctions.FileLength(filename);

            for (int i = 0; i < stringsAmount; i++) {
                stringsList.Add(RutonyBotFunctions.FileStringAt(filename, i));
            }

            return stringsList;
        }
    }
}