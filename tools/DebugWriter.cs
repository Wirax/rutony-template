using System;
using System.IO;

namespace RutonyChat.tools {
    public class DebugWriter {
        private const string LOGS_FILE = "/logs.txt";
        private static string PATH_TO_LOGS_FILE = ProgramProps.dir_scripts + "/" + LOGS_FILE;

        public static void writeError(String error) {
            if (!File.Exists(LOGS_FILE))
                File.Create(LOGS_FILE);
            else {
                File.WriteAllBytes(LOGS_FILE, new byte[0]);
            }
            
            File.WriteAllLines(PATH_TO_LOGS_FILE, new String[] {error});
        }
    }
}