﻿using System;
using System.Collections.Generic;

namespace RutonyChat {
    public class SongRequestsControl {
        public static List<RutonyBot.SongRequestItem> ListSongRequests = new List<RutonyBot.SongRequestItem>();
        public static List<RutonyBot.SongRequestItem> BlackListSongRequests = new List<RutonyBot.SongRequestItem>();

        public static List<string> ListSkipUsers = new List<string>();
        
        public static string requestname = "";
        public static string songrequestlink = "";

        public static int SkipSongCount = 0;
        public static bool isPause = true;
        public static bool isEnded = true;

        public static bool AddSongRequest(string id, string username) {
            return true;
        }

        public static void SendPlay() {
        }

        public static void SendPause() {
        }

        public static void SkipTrack() {
        }

        public static void SendVolume(int volume) {
        }

        public static void ClearList() {
        }

        public static void SaveBlacklist() {
        }

        public static void SaveRequests() {
        }

    }
}