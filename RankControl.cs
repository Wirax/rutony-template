﻿using System.Collections.Generic;

namespace RutonyChat {
    public static class RankControl {

        public class RankCondition {
            public int IndCondition = 0;
            public bool Active = false;

            public string Rank = "";
            public int RankOrder = -1;
            public string RankImage = "";
            public string RankImagePreview = "";
            public bool CondMessage = false;
            public int MessageQty = 0;
            public bool CondSubscribe = false;
            public int SubscribeQty = 0;
            public bool CondDonate = false;
            public float DonateQty = 0;
            public bool CondCredits = false;
            public float CreditsQty = 0;
            public bool CondTime = false;
            public float TimeQty = 0;
        }

        public static List<RankCondition> ListConditions;

        public class ChatterRank {
            //public ProgramProps.SiteEnum Site;
            public string Nickname = "";
            public string DisplayName = "";
            public string Rank = "";
            public int RankOrder = -1;
            public string RankImage = "";
            public int MessageQty = 0;
            public int SubscribeQty = 0;
            public float DonateQty = 0;
            public int IndCondition = 0;
            public int CreditsQty = 0;
            public bool CondTime = false;
            public int TimeQty = 0;
            public int TimeToCredits = 0;
            public int MessagesToCredits = 0;
        }

        public static List<ChatterRank> ListChatters = new List<ChatterRank>();

    }
}
