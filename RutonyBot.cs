﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RutonyChat {
    public static class RutonyBot {

        public static bool Active = false;

        public class SongRequestItem {
            public string RequestedBy = "";
            public string Id = "";
            public string Duration = "";
            public DateTime DurationDT;
            public string Title = "";
            public int viewCount = 0;
            public int likeCount = 0;
            public int dislikeCount = 0;
        }

        public static List<SongRequestItem> ListSongRequests = new List<SongRequestItem>();
        public static List<SongRequestItem> BlackListSongRequests = new List<SongRequestItem>();

        public static void SayToWindow(string text) {

            Console.WriteLine(string.Format("SayToWindow: {0}", text));

        }

        public static void BotSay(string site, string text) {

            Console.WriteLine(string.Format("Say: site [{0}], text [{1}]", site, text));

        }

        public static SongRequestItem GetVideoId(string videoId) {

            return new SongRequestItem();
        }

        public static class GoodgameBot {
            public static void SendCommand(String command) {
            }
            
            public static void SendBan(String username, int seconds) {
            }
        }
        
        public static class TwitchBot {
            public static void SendCommand(String command) {
            }

            public static void SendBan(String name, int seconds) {
                
            }
        }


    }
}
